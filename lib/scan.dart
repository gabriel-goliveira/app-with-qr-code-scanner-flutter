import 'package:flutter/material.dart';
import 'package:barcode_scan/barcode_scan.dart';
import 'package:flutter/services.dart';


class ScanScreen extends StatefulWidget {
  @override
  _ScanScreenState createState() => _ScanScreenState();
}

class _ScanScreenState extends State<ScanScreen> {

  String result = "Your Information";
  int veri = 0;
  List resultado;

  Future _scanQR() async{
    try{
      String qrResult = await BarcodeScanner.scan();
      setState(() {
        result = qrResult;
        if(result.contains("\n"))
        resultado = result.split("\n");
      });
    } on PlatformException catch(ex) {
      if(ex.code == BarcodeScanner.CameraAccessDenied){
        setState(() {
          result = "Camera Permission is denied";
        });
      }else{
        setState(() {
          result = "Unknown error $ex";
        });
      }
    } on FormatException {
      setState(() {
        result = "You pressed the back button before scanning anything";
      });
    } catch(ex){
      setState(() {
        result = "Unknown error $ex";
      });
    }
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("QR Code"),
        centerTitle: true,
      ),
      floatingActionButton: FloatingActionButton.extended(
        onPressed: (){
          _scanQR();
          veri++;
        },
        label: Text("Scan"),
        icon: Icon(Icons.camera_alt),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      body: SingleChildScrollView(
        child: Center(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              resultado?.isEmpty ?? true ?
              Text(
                result,
                style: TextStyle(fontSize: 30.0, fontWeight: FontWeight.bold),
              ) :
              FutureBuilder(
                  builder: (context, snapshot){
                    return Column(
                      children: <Widget>[
                        Text(
                          resultado[0]== null ? "" : resultado[0],
                          style: TextStyle(fontSize: 30.0, fontWeight: FontWeight.bold, color: Colors.red),
                        ),
                        Text(
                          resultado[1]== null ? "" : resultado[1],
                          style: TextStyle(fontSize: 30.0, fontWeight: FontWeight.bold, color: Colors.blue),
                        ),
                        Text(
                          resultado[2]== null ? "" : resultado[2],
                          style: TextStyle(fontSize: 30.0, fontWeight: FontWeight.bold, color: Colors.black),
                        )
                      ],
                    );
                  }
              ),
            ],
          ),
        ),
      ),
    );
  }
}
