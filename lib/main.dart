import 'package:app_qrcode/scan.dart';
import 'package:flutter/material.dart';
import 'package:qr_flutter/qr_flutter.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  Map<String, dynamic> userData = {
    "nome" : "Gabriel Gomes",
    "idade" : 21,
    "rg": "1111111111-1"
  };

  List<dynamic> infos;

  @override
  void initState() {

  }




  @override
  Widget build(BuildContext context) {

    String teste = userData["nome"] + "\n" + userData["idade"].toString() + "\n" + userData["rg"];
    List teste2 = teste.split("\n");


    return Scaffold(
      appBar: AppBar(
        title: Text("QR Code"),
        centerTitle: true,
      ),
      body: SingleChildScrollView(
        child: Center(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                'Seu QR Code ' + teste2[0],
              ),
              Padding(padding: EdgeInsets.only(top: 20.0, bottom: 20.0),
                child: QrImage(data: teste),
              ),
            ],
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: (){
          Navigator.of(context).push(MaterialPageRoute(builder: (context) => ScanScreen()            ));
        },
      ),
    );
  }


}
